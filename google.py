from random import random
from bs4 import BeautifulSoup
from requests import get
import csv
import time
import random

usr_agent = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}
def _req(term, results, lang, start):
    resp = get(
        url="https://www.google.com/search",
        headers=usr_agent,
        params=dict(
            q = term,
            num = results + 2, # Prevents multiple requests
            # num = 100,
            hl = lang,
            start = start,
        )
    )
    resp.raise_for_status()
    return resp

def search(term, num_results=1000, lang="en", proxy=None, advanced=False):
    escaped_term = term.replace(' ', '+')
    start = 0
    while start < num_results:
        # Send request
        # resp = _req(escaped_term, num_results-start, lang, start)
        resp = _req(escaped_term, num_results-start, lang, start)
        # Parse
        soup = BeautifulSoup(resp.text, 'html.parser')
        # print(resp.text)
        result_block = soup.find_all('div', attrs={'class': 'g'})
        for result in result_block:
            # Find link, title, description   
            link = result.find('a', href=True)
            link = link.attrs['href']
            title = result.find('h3')
            title = title.text
            description_box = result.find('div', {'style': '-webkit-line-clamp:2'})
            if description_box:
                description_box = description_box.text
            else:
                description_box = ''
            print(title + ',' + link + ',' + description_box)
            # write the data
            data = [title,link,description_box]
            writer.writerow(data)
            start += 1
        r = random.randint(5,10)
        print(r)
        time.sleep(r)
            
# header = ['name', 'area', 'country_code2', 'country_code3']
# data = ['Afghanistan', 652090, 'AF', 'AFG']

header = ['Title', 'link', 'Description']
f = open('or.th.csv', 'w',encoding='UTF8', newline='')
writer = csv.writer(f)
writer.writerow(header)            
search('site:or.th')
f.close()
    
    